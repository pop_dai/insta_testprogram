from keras.applications.resnet50 import ResNet50
from keras.preprocessing import image
from keras.applications.resnet50 import preprocess_input, decode_predictions
import numpy as np
import time
import os
import re
from glob import glob

model = ResNet50(weights='imagenet')
directory = os.listdir('C:/log/img')

# 最新の10件を取得
def get_latest_file(dirname):
  target = os.path.join(dirname, '*')
  files = [(f, os.path.getmtime(f)) for f in glob(target)]
  get_latest_file = sorted(files, key=lambda files: files[1])[::-1][0:10]
  return get_latest_file

if __name__ == '__main__':
  dirname = "C:/log/img"

list_latest_file = get_latest_file(dirname)

# 新たな配列に必要な情報を追加
newlist = []
for r in range(10):
	list_scape = list_latest_file[r][0]
	print(list_scape)
	newlist.append(list_scape)

directory_name = get_latest_file("C:/log/img")

for i in newlist:
	img_path = str(i) + '/material.jpg'
	os.path.exists(img_path)
	if(os.path.exists(img_path)):
		img = image.load_img(img_path, target_size=(224, 224))
		img

		x = image.img_to_array(img)
		x = np.expand_dims(x, axis=0)
		x = preprocess_input(x)
		preds = model.predict(x)

		list = decode_predictions(preds, top=3)[0]

		test0 = list[0]
		test1 = list[1]
		test2 = list[2]

		path_w = i + '/material.txt'
		print(path_w)
		from googletrans import Translator
		translator = Translator()
		a = translator.translate(test0[1], dest='ja', src='en')
		b = translator.translate(test1[1], dest='ja', src='en')
		c = translator.translate(test2[1], dest='ja', src='en')

		with open(path_w, mode='a',encoding="utf-8") as f:
			f.write(a.text + ',' + b.text + ',' + c.text + ',')

time.sleep(3)
print('fin')