'use strict'

const puppeteer = require('puppeteer');
const fs = require('fs');

//chatwork
const postChatworkMessage = require('post-chatwork-message');
const CHATWORK_API_KEY = 'cbab7a21a70e24c1bc41619179414f4c';
const roomId = '';

var waitTime = 3000;
var insta_id = process.argv[2];
var insta_pass = process.argv[3];
var target_name = process.argv[4];
var waitTime_iine = parseInt(process.argv[5]);

//ファイルへの追記関数
function appendFile(path, data) {
    fs.appendFile(path, data, function(err) {
        if (err) { throw err; }
    });
}

//現在時刻の取得
function Gettime() {
    var now = new Date();
    var year = now.getYear();
    var month = now.getMonth() + 1;
    var day = now.getDate();
    var hour = now.getHours();
    var min = now.getMinutes();
    var sec = now.getSeconds();
    if (year < 2000) { year += 1900; }
    if (month < 10) { month = "0" + month; }
    if (day < 10) { day = "0" + day; }
    if (hour < 10) { hour = "0" + hour; }
    if (min < 10) { min = "0" + min; }
    if (sec < 10) { sec = "0" + sec; }
}

const options = {
    viewport: { width: 320, height: 580, },
    userAgent: "Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1",
};

puppeteer.launch({
    headless: false,
    slowMo: 100
}).then(async browser => {
    const page = await browser.newPage();
    await page.goto('https://www.instagram.com/accounts/login/');
    await page.waitFor(waitTime);
    await page.type('input[name="username"]', insta_id);
    await page.type('input[name="password"]', insta_pass);
    await page.click('.Igw0E.IwRSH.eGOV_._4EzTm.bkEs3.CovQj.jKUp7.DhRcB');
    await page.waitFor(waitTime);
    const login_url = await page.url();

    //セキュリティコードを求められたときに待機
    if (login_url.match(/challenge/)) {
        console.log('セキュリティコードの入力を要求されています');
        postChatworkMessage(CHATWORK_API_KEY, roomId, 'アカウント名「' + target_name + '」がセキュリティコードの入力を要求されています');
        while (login_url.match(/challenge/)) {
            const login_url_check = await page.url();
            if (!login_url_check.match(/challenge/)) { break; }
            await page.waitFor(10000);
        }
    }

    //ログのループ回数を取得し、数値型
    const file_read = fs.readFileSync('c:\\log\\' + target_name + '_log.txt', 'utf8');
    var f_index = file_read.indexOf(',');
    var f_str = file_read.substring(0, f_index);
    var loopNumber = parseInt(f_str) || 0;

    var csvData = [];
    const preloadFile = fs.readFileSync('c:\\list\\' + target_name + '_list.csv', 'utf8');
    var lines = preloadFile.split('\n');

    for (var i = 0; i < lines.length; i++) {
        var cells = lines[i].split(',');
        if (cells.length != 1) {
            csvData.push(cells);
        }
    }
    var all_list_count = lines.length;

    for (i in csvData) {
        var ii = parseInt(i) + loopNumber + 1;
        var url = csvData[ii][0];
        await page.goto(url);
        await page.waitFor(waitTime);

        //投稿写真の有無を判断
        var datas = await page.evaluate((selector) => {
            const list = Array.from(document.querySelectorAll('.v1Nh3.kIKUG._bz0w > a'));
            return list.map(data => data.href);
        });

        if (datas.length === 0) {
            console.log('not found pict :' + url);
        } else {
            await page.waitFor(waitTime);
            var str_url = String(url);
            var user_name_url = str_url.slice(26);
            console.log(user_name_url);

            //ユーザーごとに写真を格納するフォルダを作成
            await fs.mkdir('C:\\log\\img' + '\\' + user_name_url, { recursive: true }, (err) => {
                if (err) throw err;
            });

            await page.goto(datas[0]);
            await page.waitFor(waitTime);
            await get_pic();

            //写真を取得
            async function get_pic() {
                await page.on('response', async (response) => {
                    const matche_url = response.url();
                    if (matche_url.match(/jpg/)) {
                        const buffer = await response.buffer();
                        fs.writeFileSync('C:\\log\\img' + '\\' + user_name_url + `/material.jpg`, buffer, 'base64');
                    }
                });
                await page.waitFor(5000);
                console.log(page.url());
                //投稿ページのURLのみ別ファイルで管理
                appendFile('c:\\log\\' + target_name + '_before_pyurl.csv', user_name_url + ',' + page.url() + '\r\n');
            }

            //10件カウント
            let count_get = await page.evaluate(() => {
                now_count_data = localStorage.getItem('count_flg') || 0;
                var int_ct = parseInt(now_count_data);
                int_ct++;
                localStorage.setItem('count_flg', int_ct);
                return int_ct;
            });

            if (count_get == 10) {
                console.log("start.bat");
                await exec_bat();
                const count_reset = await page.evaluate(() => {
                    localStorage.setItem('count_flg', 0);
                });
            } else {
                console.log(count_get);
            }

            // AIで写真を判断
            async function exec_bat() {
                const execSync = require('child_process').execSync;
                exec_bat_py = execSync("start.bat");
                console.log("start.bat実行");
                await page.waitFor(5000);
                await get_pyfile_info();
            }

            async function get_pyfile_info() {
                //最新の10件を取得
                const pyurl_read = fs.readFileSync('c:\\log\\' + target_name + '_before_pyurl.csv', 'utf8');
                var pyurl_arr = pyurl_read.split('\r\n');
                pyurl_arr.pop();
                const exec_py_ary = pyurl_arr.slice(-10);

                user_info = {};
                ng_info = [];
                post_url = [];

                async function ready(element, index, array) {
                    var arr_info = element.indexOf(',');
                    var user_url = element.substring(0, arr_info); //ユーザー名のみ取得
                    var post_url_set = element.slice(arr_info + 1); //投稿URLのみ取得
                    post_url.push(post_url_set);
                    user_info[user_url] = post_url_set; //連想配列に追加
                }
                exec_py_ary.forEach(ready);
                await page.waitFor(1000);

                for (var i = 0; i < post_url.length; i++) {
                    await console.log(post_url[i]);
                    await page.goto(post_url[i]);
                    await page.waitFor(waitTime);

                    //いいねボタンのクラス取得
                    const get_btn_class = await page.evaluate(() => {
                        const node = document.querySelectorAll("button");
                        const array = [];
                        for (item of node) {
                            array.push(item.getAttribute("class"));
                        }
                        return array[1];
                    });

                    //クリック時の文字の取得
                    const get_chara_iine = await page.evaluate(() => {
                        const node = document.querySelectorAll("span");
                        const array = [];
                        for (item of node) {
                            array.push(item.getAttribute("aria-label"));
                        }
                        return array[3];
                    });

                    try {
                        //スペースを置換
                        var transform_val = get_btn_class;
                        var result = transform_val.replace(' ', '.');
                        while (result !== transform_val) {
                            transform_val = transform_val.replace(' ', '.');
                            result = result.replace(' ', '.');
                        }
                        var btn_class_new = '.' + result;

                        if (get_chara_iine == 'いいね！') {
                            await page.click(btn_class_new);
                            await page.waitFor(waitTime);
                            await page.on('response', response => {
                                if (400 == response.status()) {
                                    const response_false = await page.evaluate(() => {
                                        localStorage.setItem('response_flg', 1);
                                    });
                                };
                            });
                        } else if (get_chara_iine == '「いいね！」を取り消す') {
                            await page.waitFor(waitTime);
                            throw '「いいね！」はすでに押されています';
                        } else {
                            await page.waitFor(waitTime);
                            throw '「いいね！」もしくは「いいね！を取り消す」の値が取れません';
                        }

                        const response_false_check = await page.evaluate(() => {
                            data = localStorage.getItem('response_flg');
                            return data;
                        });

                        if (response_false_check == 1) {
                            await page.waitFor(14400000);
                            await postChatworkMessage(CHATWORK_API_KEY, roomId, 'アカウント名「' + target_name + '」 status 400 処理を4時間中断');
                            await console.log('処理を4時間中断しました');
                        }

                        let gettime = new Gettime;
                        var filename = ii + ',' + year + '/' + month + '/' + day + ' ' + hour + ':' + min + ':' + sec + '   ' + url;
                        await fs.writeFileSync('c:\\log\\' + target_name + '_log.txt', filename);
                        await appendFile('c:\\log\\' + target_name + '_exec_log.csv', element + ',' + year + '/' + month + '/' + day + ',' + hour + ':' + min + ':' + sec + '\r\n');
                        await console.log(element);

                        const c_file_read = fs.readFileSync('c:\\log\\' + target_name + '_log.txt', 'utf8');
                        var c_index = c_file_read.indexOf(',');
                        var c_str = c_file_read.substring(0, c_index);

                        var count_now = parseInt(c_str) + 1;
                        var all_list_count_op = parseInt(all_list_count);
                        await console.log('list' + '' + target_name + ' ' + all_list_count_op + '件 / ' + count_now + '件');
                        await page.waitFor(waitTime_iine);
                    } catch (e) {
                        console.log(e);
                        let gettime = new Gettime;
                        appendFile('c:\\log\\' + target_name + '_error_log.txt', year + '/' + month + '/' + day + ' ' + hour + ':' + min + ':' + sec + '\r\n' + e + '\r\n');
                    }
                }
            }
        }
    }
});